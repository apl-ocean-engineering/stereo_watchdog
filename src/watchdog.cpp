//
//
//
// Copyright 2025 University of Washington
//
//

#include <diagnostic_updater/diagnostic_updater.h>
#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>
#include <ros/timer.h>
#include <sensor_msgs/CameraInfo.h>
#include <stdint.h>

#include <iomanip>  // for std::setprecision and std::fixed
#include <list>
#include <mutex>
#include <string>

#include "nodelet/nodelet.h"

namespace stereo_watchdog {

enum class CameraSide { LEFT_SIDE, RIGHT_SIDE };

const std::string side_to_str(const CameraSide i) {
  switch (i) {
    case CameraSide::LEFT_SIDE:
      return "LEFT";
      break;
    case CameraSide::RIGHT_SIDE:
      return "RIGHT";
      break;
  }

  return "(unknown)";
}

class StereoWatchdog : public nodelet::Nodelet {
 public:
  struct Frame {
    double time;
    uint32_t seq;
    std::string frame;

    bool sequential;

    Frame() = delete;

    explicit Frame(const sensor_msgs::CameraInfoConstPtr &msg)
        : time(time = msg->header.stamp.toSec()),
          seq(msg->header.seq),
          frame(msg->header.frame_id) {
      if (seq > 0) {
        sequential = (msg->header.seq == (seq + 1));
      } else {
        sequential = true;
      }
    }
  };
  typedef std::list<Frame> FrameQueue;

  StereoWatchdog() : max_delta_(0.02) {}

 private:
  virtual void onInit() {
    ros::NodeHandle nh = getNodeHandle();
    ros::NodeHandle pnh = getPrivateNodeHandle();

    // Get max delta
    if (!pnh.getParam("max_sync_delta", max_delta_)) {
      NODELET_FATAL_STREAM(
          "Could not get parameter max_sync_delta, using default "
          << max_delta_);
    }

    // Create subscriber objects
    const auto hints = ros::TransportHints().reliable().tcpNoDelay();
    left_sub_ = nh.subscribe<sensor_msgs::CameraInfo>(
        "left_camera_info", 10,
        boost::bind(&StereoWatchdog::inspectCameraInfo, this,
                    CameraSide::LEFT_SIDE, boost::placeholders::_1),
        ros::VoidConstPtr(), hints);
    right_sub_ = nh.subscribe<sensor_msgs::CameraInfo>(
        "right_camera_info", 10,
        boost::bind(&StereoWatchdog::inspectCameraInfo, this,
                    CameraSide::RIGHT_SIDE, boost::placeholders::_1),
        ros::VoidConstPtr(), hints);

    updater_.setHardwareID("none");
    periodic_timer_ =
        nh.createTimer(ros::Duration(1.0),
                       boost::bind(&StereoWatchdog::periodicTask, this, _1));
  }

  void periodicTask(const ros::TimerEvent &event) { updater_.update(); }

  void analyzeQueues() {
    while ((left_queue_.size() > 0) && (right_queue_.size() > 0)) {
      const Frame &left_frame(left_queue_.front());
      const Frame &right_frame(right_queue_.front());

      double diff = fabs(left_frame.time - right_frame.time);
      NODELET_DEBUG_STREAM(left_frame.seq
                           << " (" << std::setprecision(11) << std::fixed
                           << left_frame.time << ") "
                           << " ; " << right_frame.seq << " ("
                           << right_frame.time << ") "
                           << " == " << std::setprecision(6) << diff * 1000
                           << " ms");

      if (left_frame.frame == right_frame.frame) {
        NODELET_FATAL_STREAM("Frames from same side: " << left_frame.frame);
        left_queue_.pop_front();
        right_queue_.pop_front();
      } else if (diff > max_delta_) {
        NODELET_WARN_STREAM("Frames out-of-sync by " << diff * 1000.0 << " ms");

        // Invalidate the older one
        if (left_frame.time < right_frame.time) {
          left_queue_.pop_front();
        } else {
          right_queue_.pop_front();
        }

      } else {
        // On good match... drop the matched images and try again
        left_queue_.pop_front();
        right_queue_.pop_front();
      }
    }
  }

  std::mutex cb_mutex;

  void inspectCameraInfo(CameraSide side,
                         const sensor_msgs::CameraInfoConstPtr &msg) {
    std::lock_guard<std::mutex> guard(cb_mutex);

    const int max_queue_len = 10;

    if (side == CameraSide::LEFT_SIDE) {
      left_queue_.push_back(Frame(msg));
      while (left_queue_.size() > max_queue_len) left_queue_.pop_front();
    } else if (side == CameraSide::RIGHT_SIDE) {
      right_queue_.push_back(Frame(msg));
      while (right_queue_.size() > max_queue_len) right_queue_.pop_front();
    }

    analyzeQueues();
  }

  FrameQueue left_queue_, right_queue_;

  double max_delta_;
  const double min_frame_period = 0.04;

  ros::Subscriber left_sub_, right_sub_;
  diagnostic_updater::Updater updater_;

  ros::Timer periodic_timer_;
};

}  // namespace stereo_watchdog

PLUGINLIB_EXPORT_CLASS(stereo_watchdog::StereoWatchdog, nodelet::Nodelet);
