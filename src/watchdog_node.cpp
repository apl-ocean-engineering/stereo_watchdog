//
//
//
// Copyright 2025 University of Washington
//
//

#include <string>

#include "nodelet/loader.h"
#include "ros/ros.h"

int main(int argc, char **argv) {
  ros::init(argc, argv, "watchdog_node");
  nodelet::Loader nodelet;
  nodelet::M_string remap(ros::names::getRemappings());
  nodelet::V_string nargv;
  std::string nodelet_name = ros::this_node::getName();

  // Run the nodelet "stereo_watchdog"
  nodelet.load(nodelet_name, "stereo_watchdog/stereo_watchdog", remap, nargv);

  // Let ROS take over
  ros::spin();
}
