from setuptools import setup

setup(
    name="stereo_watchdog",
    version="0.1.0",
    install_requires=[
        "rospy",
        "sensor_msgs",
    ],
    scripts=[
        "scripts/binning_snitch",
    ],
)
